/* QcMatrix: an abstract matrix library
   Copyright 2012-2015 Bin Gao

   QcMatrix is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   QcMatrix is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with QcMatrix. If not, see <http://www.gnu.org/licenses/>.

   This file tests the function QcMatZeroEntries().

   2014-03-28, Bin Gao:
   * first version
*/

/* header file of QcMatrix library */
#include "qcmatrix.h"
/* parameters for test suite */
#include "tests/qcmatrix_test_param.h"

/*% \brief tests the function QcMatZeroEntries()
    \author Bin Gao
    \date 2014-03-28
    \param[QcMat:type]{in} A the matrix
*/
QErrorCode test_c_QcMatZeroEntries(QcMat *A)
{
    QcMat B;             /* duplication of the matrix A */
    QBool assembled;     /* indicates if the matrix is assembled or not */
    QInt num_blocks;     /* number of blocks */
    QInt dim_mat;        /* dimension of each block */
    QInt size_values;    /* number of elements in the matrix */
    QReal *values_real;  /* values of the real part */
    const QReal CF_THRESHOLD=1000.0*QZEROTHRSH;
    QBool is_equal;      /* indicates if the matrix and array have the same values */
    QInt ival;           /* incremental recorder over values */
    QErrorCode ierr;     /* error information */
#if defined(QCMATRIX_ENABLE_VIEW)
    FILE *fp_mat;
#endif
    /* checks if the matrix is assembled */
    ierr = QcMatIsAssembled(A, &assembled);
    QErrorCheckCode(ierr, FILE_AND_LINE, "calling QcMatIsAssembled(A)");
    if (assembled!=QTRUE) {
        printf("test_c_QcMatZeroEntries>> matrix A is not assembled ...\n");
        printf("test_c_QcMatZeroEntries>> QcMatZeroEntries() will not be tested ...\n");
        return QSUCCESS;
    }
    /* duplicates the matrix, and uses duplication for the test */
    ierr = QcMatCreate(&B);
    QErrorCheckCode(ierr, FILE_AND_LINE, "calling QcMatCreate(B)");
    ierr = QcMatDuplicate(A, COPY_PATTERN_AND_VALUE, &B);
    QErrorCheckCode(ierr, FILE_AND_LINE, "calling QcMatDuplicate(A, COPY_PATTERN_AND_VALUE)");
    /* gets the number of blocks */
    ierr = QcMatGetNumBlocks(&B, &num_blocks);
    QErrorCheckCode(ierr, FILE_AND_LINE, "calling QcMatGetNumBlocks(B)");
    /* gets the dimension of each block */
    ierr = QcMatGetDimMat(&B, &dim_mat, &dim_mat);
    QErrorCheckCode(ierr, FILE_AND_LINE, "calling QcMatGetDimMat(B)");
    size_values = num_blocks*num_blocks*dim_mat*dim_mat;
    /* allocates memory for the elements of the matrix */
    values_real = (QReal *)malloc(sizeof(QReal)*size_values);
    if (values_real==NULL) {
        printf("test_c_QcMatZeroEntries>> failed to allocate values_real\n");
        return QFAILURE;
    }
    for (ival=0; ival<size_values; ival++) {
        values_real[ival] = 0;
    }
    /* zeros all entries of the matrix by QcMatZeroEntries() */
    ierr = QcMatZeroEntries(&B);
    QErrorCheckCode(ierr, FILE_AND_LINE, "calling QcMatZeroEntries(B)");
    ierr = QcMatCfArray(&B,
                        QFALSE,
                        size_values,
                        values_real,
                        values_real,
                        CF_THRESHOLD,
                        &is_equal);
    QErrorCheckCode(ierr, FILE_AND_LINE, "calling QcMatCfArray(B)");
    if (is_equal==QTRUE) {
        printf("test_c_QcMatZeroEntries>> QcMatZeroEntries(B) passed ...\n");
    }
    else {
        /* dumps results to check */
#if defined(QCMATRIX_ENABLE_VIEW)
        fp_mat = fopen("test_c_QcMatZeroEntries.yml", "w");
        if (fp_mat==NULL) {
            QErrorExit(FILE_AND_LINE, "failed to open test_c_QcMatZeroEntries.yml");
        }
        ierr = QcMatWrite(&B, fp_mat, ASCII_VIEW);
        QErrorCheckCode(ierr, FILE_AND_LINE, "calling QcMatWrite(B)");
        fclose(fp_mat);
#endif
        printf("test_c_QcMatZeroEntries>> QcMatZeroEntries(B) failed\n");
        return QFAILURE;
    }
    /* cleans */
    free(values_real);
    values_real = NULL;
    ierr = QcMatDestroy(&B);
    QErrorCheckCode(ierr, FILE_AND_LINE, "calling QcMatDestroy(B)");
    return QSUCCESS;
}
