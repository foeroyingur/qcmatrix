/* QcMatrix: an abstract matrix library
   Copyright 2012-2015 Bin Gao

   QcMatrix is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   QcMatrix is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with QcMatrix. If not, see <http://www.gnu.org/licenses/>.

   This file tests the function QcMatDuplicate().

   2014-03-25, Bin Gao:
   * first version
*/

/* header file of QcMatrix library */
#include "qcmatrix.h"
/* parameters for test suite */
#include "tests/qcmatrix_test_param.h"

/*% \brief tests the function QcMatDuplicate()
    \author Bin Gao
    \date 2014-03-25
    \param[QcMat:type]{in} A the matrix
*/
QErrorCode test_c_QcMatDuplicate(QcMat *A)
{
    QcMat B;          /* duplicated matrix */
    const QReal CF_THRESHOLD=1000.0*QZEROTHRSH;
    QBool is_equal;   /* indicates if two matrices are equal (pattern and values) */
    QErrorCode ierr;  /* error information */
#if defined(QCMATRIX_ENABLE_VIEW)
    FILE *fp_mat;
#endif
    /* creates the duplicated matrix first */
    ierr = QcMatCreate(&B);
    QErrorCheckCode(ierr, FILE_AND_LINE, "calling QcMatCreate(B)");
    /* tests QcMatDuplicate() with the option COPY_PATTERN_ONLY */
    ierr = QcMatDuplicate(A, COPY_PATTERN_ONLY, &B);
    QErrorCheckCode(ierr, FILE_AND_LINE, "calling QcMatDuplicate(A, COPY_PATTERN_ONLY, B)");
    ierr = QcMatIsEqual(A, &B, QFALSE, CF_THRESHOLD, &is_equal);
    QErrorCheckCode(ierr, FILE_AND_LINE, "calling QcMatIsEqual(A, B)");
    if (is_equal==QTRUE) {
        printf("test_c_QcMatDuplicate>> QcMatDuplicate(A, COPY_PATTERN_ONLY) passed ...\n");
    }
    else {
        printf("test_c_QcMatDuplicate>> QcMatDuplicate(A, COPY_PATTERN_ONLY) failed\n");
        return QFAILURE;
    }
    /* tests QcMatDuplicate() with the option COPY_PATTERN_AND_VALUE */
    ierr = QcMatDuplicate(A, COPY_PATTERN_AND_VALUE, &B);
    QErrorCheckCode(ierr, FILE_AND_LINE, "calling QcMatDuplicate(A, COPY_PATTERN_AND_VALUE, B)");
    ierr = QcMatIsEqual(A, &B, QTRUE, CF_THRESHOLD, &is_equal);
    QErrorCheckCode(ierr, FILE_AND_LINE, "calling QcMatIsEqual(A, B)");
    if (is_equal==QTRUE) {
        printf("test_c_QcMatDuplicate>> QcMatDuplicate(A, COPY_PATTERN_AND_VALUE) passed ...\n");
    }
    else {
    /* dumps results to check */
#if defined(QCMATRIX_ENABLE_VIEW)
        fp_mat = fopen("test_c_QcMatDuplicate.yml", "w");
        if (fp_mat==NULL) {
            QErrorExit(FILE_AND_LINE, "failed to open test_c_QcMatDuplicate.yml");
        }
        ierr = QcMatWrite(A, fp_mat, ASCII_VIEW);
        QErrorCheckCode(ierr, FILE_AND_LINE, "calling QcMatWrite(A)");
        ierr = QcMatWrite(&B, fp_mat, ASCII_VIEW);
        QErrorCheckCode(ierr, FILE_AND_LINE, "calling QcMatWrite(B)");
        fclose(fp_mat);
#endif
        printf("test_c_QcMatDuplicate>> QcMatDuplicate(A, COPY_PATTERN_AND_VALUE) failed\n");
        return QFAILURE;
    }
    /* frees the space taken by the matrix B */
    ierr = QcMatDestroy(&B);
    QErrorCheckCode(ierr, FILE_AND_LINE, "calling QcMatDestroy(B)");
    return QSUCCESS;
}
