/* QcMatrix: an abstract matrix library
   Copyright 2012-2015 Bin Gao

   QcMatrix is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   QcMatrix is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with QcMatrix. If not, see <http://www.gnu.org/licenses/>.

   This file tests the function QcMatGetTrace().

   2014-03-28, Bin Gao:
   * first version
*/

/* header file of QcMatrix library */
#include "qcmatrix.h"
/* some basic algebraic functions */
#include "utilities/qcmatrix_algebra.h"
/* parameters for test suite */
#include "tests/qcmatrix_test_param.h"

/*% \brief tests the function QcMatGetTrace()
    \author Bin Gao
    \date 2014-03-28
    \param[QcMat:type]{in} A the matrix
*/
QErrorCode test_c_QcMatGetTrace(QcMat *A)
{
    QInt num_blocks;        /* number of blocks */
    QInt *idx_block_row;    /* row indices of the blocks */
    QcDataType *data_type;  /* data types of the blocks */
    QInt dim_mat;           /* dimension of each block */
    QInt size_mat;          /* number of elements in each block */
    QInt idx_first_row;     /* index of the first row to get values */
    QInt num_row_get;       /* number of rows to get */
    QInt idx_first_col;     /* index of the first column to get values */
    QInt num_col_get;       /* number of columns to get */
    QReal *values_real;     /* values of the real part */
    QReal *values_imag;     /* values of the imaginary part */
    QReal *cf_trace;        /* trace to compare with */
    QReal *trace;           /* trace from QcMatGetTrace() */
    const QReal CF_THRESHOLD=1000.0*QZEROTHRSH;
    QInt iblk, jblk, kblk;  /* incremental recorders for blocks */
    QInt ival, jval;        /* incremental recorders for elements of each block */
    QErrorCode ierr;        /* error information */
    /* gets the number of blocks */
    ierr = QcMatGetNumBlocks(A, &num_blocks);
    QErrorCheckCode(ierr, FILE_AND_LINE, "calling QcMatGetNumBlocks(A)");
    /* gets the data types of the diagonal blocks */
    idx_block_row = (QInt *)malloc(sizeof(QInt)*num_blocks);
    if (idx_block_row==NULL) {
        printf("test_c_QcMatGetTrace>> failed to allocate idx_block_row\n");
        return QFAILURE;
    }
    data_type = (QcDataType *)malloc(sizeof(QcDataType)*num_blocks);
    if (data_type==NULL) {
        printf("test_c_QcMatGetTrace>> failed to allocate data_type\n");
        return QFAILURE;
    }
    for (iblk=0; iblk<num_blocks; iblk++) {
#if defined(QCMATRIX_ZERO_BASED)
        idx_block_row[iblk] = iblk;
#else
        idx_block_row[iblk] = iblk+1;
#endif
    }
    ierr = QcMatGetDataType(A, num_blocks, idx_block_row, idx_block_row, data_type);
    QErrorCheckCode(ierr, FILE_AND_LINE, "calling QcMatGetDataType(A)");
    /* gets the dimension of each block */
    ierr = QcMatGetDimMat(A, &dim_mat, &dim_mat);
    QErrorCheckCode(ierr, FILE_AND_LINE, "calling QcMatGetDimMat(A)");
    size_mat = dim_mat*dim_mat;
    /* allocates memory for getting the elements of each diagonal block */
    values_real = (QReal *)malloc(sizeof(QReal)*size_mat);
    if (values_real==NULL) {
        printf("test_c_QcMatGetTrace>> failed to allocate values_real\n");
        return QFAILURE;
    }
    values_imag = (QReal *)malloc(sizeof(QReal)*size_mat);
    if (values_imag==NULL) {
        printf("test_c_QcMatGetTrace>> failed to allocate values_imag\n");
        return QFAILURE;
    }
    /* calculates the trace block by block */
    cf_trace = (QReal *)malloc(sizeof(QReal)*2*num_blocks);
    if (cf_trace==NULL) {
        printf("test_c_QcMatGetTrace>> failed to allocate cf_trace\n");
        return QFAILURE;
    }
#if defined(QCMATRIX_ZERO_BASED)
    idx_first_row = 0;
    idx_first_col = 0;
#else
    idx_first_row = 1;
    idx_first_col = 1;
#endif
    num_row_get = dim_mat;
    num_col_get = dim_mat;
    for (iblk=0; iblk<num_blocks; iblk++) {
        jblk = 2*iblk;
        kblk = 2*iblk+1;
        cf_trace[jblk] = 0;  /* real part */
        cf_trace[kblk] = 0;  /* imaginary part */
        switch (data_type[iblk]) {
        /* real block */
        case QREALMAT:
            ierr = QcMatGetValues(A,
                                  idx_block_row[iblk],
                                  idx_block_row[iblk],
                                  idx_first_row,
                                  num_row_get,
                                  idx_first_col,
                                  num_col_get,
                                  values_real,
                                  NULL);
            if (ierr==QSUCCESS) {
                jval = -dim_mat-1;
                for (ival=0; ival<dim_mat; ival++) {
                    jval += dim_mat+1;
                    cf_trace[jblk] += values_real[jval];
                }
            }
            else {
                printf("test_c_QcMatGetTrace>> real block (%"QINT_FMT", %"QINT_FMT")\n",
                       idx_block_row[iblk],
                       idx_block_row[iblk]);
                printf("test_c_QcMatGetTrace>> failed to call QcMatGetValues(A)\n");
                return QFAILURE;
            }
            break;
        /* imaginary block */
        case QIMAGMAT:
            ierr = QcMatGetValues(A,
                                  idx_block_row[iblk],
                                  idx_block_row[iblk],
                                  idx_first_row,
                                  num_row_get,
                                  idx_first_col,
                                  num_col_get,
                                  NULL,
                                  values_imag);
            if (ierr==QSUCCESS) {
                jval = -dim_mat-1;
                for (ival=0; ival<dim_mat; ival++) {
                    jval += dim_mat+1;
                    cf_trace[kblk] += values_imag[jval];
                }
            }
            else {
                printf("test_c_QcMatGetTrace>> imaginary block (%"QINT_FMT", %"QINT_FMT")\n",
                       idx_block_row[iblk],
                       idx_block_row[iblk]);
                printf("test_c_QcMatGetTrace>> failed to call QcMatGetValues(A)\n");
                return QFAILURE;
            }
            break;
        /* complex block */
        case QCMPLXMAT:
            ierr = QcMatGetValues(A,
                                  idx_block_row[iblk],
                                  idx_block_row[iblk],
                                  idx_first_row,
                                  num_row_get,
                                  idx_first_col,
                                  num_col_get,
                                  values_real,
                                  values_imag);
            if (ierr==QSUCCESS) {
                jval = -dim_mat-1;
                for (ival=0; ival<dim_mat; ival++) {
                    jval += dim_mat+1;
                    cf_trace[jblk] += values_real[jval];
                    cf_trace[kblk] += values_imag[jval];
                }
            }
            else {
                printf("test_c_QcMatGetTrace>> complex block (%"QINT_FMT", %"QINT_FMT")\n",
                       idx_block_row[iblk],
                       idx_block_row[iblk]);
                printf("test_c_QcMatGetTrace>> failed to call QcMatGetValues(A)\n");
                return QFAILURE;
            }
            break;
        default:
            break;
        }
    }
    free(idx_block_row);
    idx_block_row = NULL;
    free(data_type);
    data_type = NULL;
    free(values_real);
    values_real = NULL;
    free(values_imag);
    values_imag = NULL;
    /* gets the trace by calling QcMatGetTrace() */
    trace = (QReal *)malloc(sizeof(QReal)*2*num_blocks);
    if (trace==NULL) {
        printf("test_c_QcMatGetTrace>> failed to allocate trace\n");
        return QFAILURE;
    }
    ierr = QcMatGetTrace(A, num_blocks, trace);
    QErrorCheckCode(ierr, FILE_AND_LINE, "calling QcMatGetTrace(A)");
    for (iblk=0,jblk=0,kblk=1; iblk<num_blocks; iblk++) {
        if (QAbs(trace[jblk]-cf_trace[jblk])>CF_THRESHOLD ||
            QAbs(trace[kblk]-cf_trace[kblk])>CF_THRESHOLD) {
            printf("test_c_QcMatGetTrace>> %"QINT_FMT": (%"QREAL_FMT",%"QREAL_FMT"), (%"QREAL_FMT",%"QREAL_FMT")\n",
                   iblk,
                   trace[jblk],
                   trace[kblk],
                   cf_trace[jblk],
                   cf_trace[kblk]);
            printf("test_c_QcMatGetTrace>> QcMatGetTrace(A) failed\n");
            return QFAILURE;
        }
        jblk += 2;
        kblk += 2;
    }
    printf("test_c_QcMatGetTrace>> QcMatGetTrace(A) passed ...\n");
    free(cf_trace);
    cf_trace = NULL;
    free(trace);
    trace = NULL;
    return QSUCCESS;
}
