#!/bin/bash

# removes files and directories
rm -rv ${PATH_LIB_QCMATRIX}/tests/f90/adapter/build
rm -rv ${PATH_LIB_QCMATRIX}/tests/f90/adapter/include
rm -rv ${PATH_LIB_QCMATRIX}/tests/f90/adapter/cmake
rm -v ${PATH_LIB_QCMATRIX}/tests/f90/adapter/src/qcmatrix_blas.F90
