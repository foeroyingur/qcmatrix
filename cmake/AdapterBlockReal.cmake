# Source codes for setting/getting the external square block real matrix
SET(QCMATRIX_SRCS
    ${QCMATRIX_SRCS}
    ${LIB_QCMATRIX_PATH}/src/cmplx_mat/CmplxMatBlockCreate.c
    ${LIB_QCMATRIX_PATH}/src/cmplx_mat/CmplxMatGetNumBlocks.c
    ${LIB_QCMATRIX_PATH}/src/cmplx_mat/CmplxMatSetAdapterMat.c
    ${LIB_QCMATRIX_PATH}/src/cmplx_mat/CmplxMatGetAdapterMat.c)
