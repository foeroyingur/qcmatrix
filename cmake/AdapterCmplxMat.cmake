# Source codes for setting/getting the external complex matrix
SET(QCMATRIX_SRCS
    ${QCMATRIX_SRCS}
    ${LIB_QCMATRIX_PATH}/src/qcmat/QcMatSetAdapterMat.c
    ${LIB_QCMATRIX_PATH}/src/qcmat/QcMatGetAdapterMat.c)
