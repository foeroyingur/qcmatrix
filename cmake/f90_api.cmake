# CMake file for Fortran 90 APIs
# 2014-03-11, Bin Gao:
# * generated by tools/qcmatrix_fortran.py
# Sets Fortran compiler flags
ADD_DEFINITIONS(-DSIZEOF_VOID_P=${CMAKE_SIZEOF_VOID_P})
# Source codes of the APIs
SET(API_SRCS
    ${LIB_QCMATRIX_PATH}/src/qcmat/f90/f90_api_f.F90
    ${LIB_QCMATRIX_PATH}/src/qcmat/f90/f90_api_c.c)
# Name mangling for the functions in src/qcmat/f90/f90_api_c.c
SET(FC_MANGLING_SUB ${FC_MANGLING_SUB} f90_api_QcMatCreate)
SET(FC_MANGLING_SUB ${FC_MANGLING_SUB} f90_api_QcMatDestroy)
SET(FC_MANGLING_SUB ${FC_MANGLING_SUB} f90_api_QcMatBlockCreate)
SET(FC_MANGLING_SUB ${FC_MANGLING_SUB} f90_api_QcMatSetSymType)
SET(FC_MANGLING_SUB ${FC_MANGLING_SUB} f90_api_QcMatSetDataType)
SET(FC_MANGLING_SUB ${FC_MANGLING_SUB} f90_api_QcMatSetDimMat)
IF(QCMATRIX_STORAGE_MODE)
    SET(FC_MANGLING_SUB ${FC_MANGLING_SUB} f90_api_QcMatSetStorageMode)
ENDIF()
SET(FC_MANGLING_SUB ${FC_MANGLING_SUB} f90_api_QcMatAssemble)
SET(FC_MANGLING_SUB ${FC_MANGLING_SUB} f90_api_QcMatGetNumBlocks)
SET(FC_MANGLING_SUB ${FC_MANGLING_SUB} f90_api_QcMatGetSymType)
SET(FC_MANGLING_SUB ${FC_MANGLING_SUB} f90_api_QcMatGetDataType)
SET(FC_MANGLING_SUB ${FC_MANGLING_SUB} f90_api_QcMatGetDimMat)
IF(QCMATRIX_STORAGE_MODE)
    SET(FC_MANGLING_SUB ${FC_MANGLING_SUB} f90_api_QcMatGetStorageMode)
ENDIF()
SET(FC_MANGLING_SUB ${FC_MANGLING_SUB} f90_api_QcMatIsAssembled)
SET(FC_MANGLING_SUB ${FC_MANGLING_SUB} f90_api_QcMatSetRealValues)
SET(FC_MANGLING_SUB ${FC_MANGLING_SUB} f90_api_QcMatSetImagValues)
SET(FC_MANGLING_SUB ${FC_MANGLING_SUB} f90_api_QcMatAddRealValues)
SET(FC_MANGLING_SUB ${FC_MANGLING_SUB} f90_api_QcMatAddImagValues)
SET(FC_MANGLING_SUB ${FC_MANGLING_SUB} f90_api_QcMatGetRealValues)
SET(FC_MANGLING_SUB ${FC_MANGLING_SUB} f90_api_QcMatGetImagValues)
SET(FC_MANGLING_SUB ${FC_MANGLING_SUB} f90_api_QcMatDuplicate)
SET(FC_MANGLING_SUB ${FC_MANGLING_SUB} f90_api_QcMatZeroEntries)
SET(FC_MANGLING_SUB ${FC_MANGLING_SUB} f90_api_QcMatGetTrace)
SET(FC_MANGLING_SUB ${FC_MANGLING_SUB} f90_api_QcMatGetMatProdTrace)
IF(QCMATRIX_ENABLE_VIEW)
    SET(FC_MANGLING_SUB ${FC_MANGLING_SUB} f90_api_QcMatWrite)
ENDIF()
IF(QCMATRIX_ENABLE_VIEW)
    SET(FC_MANGLING_SUB ${FC_MANGLING_SUB} f90_api_QcMatRead)
ENDIF()
SET(FC_MANGLING_SUB ${FC_MANGLING_SUB} f90_api_QcMatScale)
SET(FC_MANGLING_SUB ${FC_MANGLING_SUB} f90_api_QcMatAXPY)
SET(FC_MANGLING_SUB ${FC_MANGLING_SUB} f90_api_QcMatTranspose)
SET(FC_MANGLING_SUB ${FC_MANGLING_SUB} f90_api_QcMatGEMM)
IF(QCMATRIX_BUILD_ADAPTER AND ADAPTER_F90_LANG)
    SET(FC_MANGLING_SUB ${FC_MANGLING_SUB} f90_api_QcMatSetAdapterMat)
    SET(FC_MANGLING_SUB ${FC_MANGLING_SUB} f90_api_QcMatGetAdapterMat)
ENDIF()
SET(FC_MANGLING_SUB ${FC_MANGLING_SUB} f90_api_QcMatMatCommutator)
SET(FC_MANGLING_SUB ${FC_MANGLING_SUB} f90_api_QcMatMatSCommutator)
SET(FC_MANGLING_SUB ${FC_MANGLING_SUB} f90_api_QcMatMatHermCommutator)
SET(FC_MANGLING_SUB ${FC_MANGLING_SUB} f90_api_QcMatMatSHermCommutator)
SET(FC_MANGLING_SUB ${FC_MANGLING_SUB} f90_api_QcMatSetRandMat)
SET(FC_MANGLING_SUB ${FC_MANGLING_SUB} f90_api_QcMatIsEqual)
SET(FC_MANGLING_SUB ${FC_MANGLING_SUB} f90_api_QcMatCfArray)
SET(FC_MANGLING_SUB ${FC_MANGLING_SUB} f90_api_QcMatGetAllValues)
