/* QcMatrix: an abstract matrix library
   Copyright 2012-2015 Bin Gao

   QcMatrix is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   QcMatrix is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with QcMatrix. If not, see <http://www.gnu.org/licenses/>.

   This file implements the function QcMatWrite().

   2012-04-04, Bin Gao:
   * first version
*/

#include "qcmatrix.h"

/*@% \brief writes a matrix to file
     \author Bin Gao
     \date 2012-04-04
     \param[QcMat:struct]{in} A the matrix, should be at least assembled by QcMatAssemble()
     \param[QcViewOption:int]{in} view_option option of writing, see file
         include/types/mat_view.h
     \return[QErrorCode:int] error information
*/
QErrorCode QcMatWrite(QcMat *A, FILE *fp_mat, const QcViewOption view_option)
{
    QInt irow, icol;
    QErrorCode err_code;
    /* checks the dimension of blocks */
    if (A->num_blocks<1) {
        printf("QcMatWrite>> dimension of blocks %"QINT_FMT"\n", A->num_blocks);
        QErrorExit(FILE_AND_LINE, "invalid dimension of blocks");
    }
    fprintf(fp_mat,
            "%s- %s: %s\n",
            QCMATRIX_YAML_INDENTATION,
            QCMAT_KEY_NAME,
            A->name);
    switch (A->sym_type) {
    case QANTISYMMAT:
        fprintf(fp_mat,
                "%s  %s: %s\n",
                QCMATRIX_YAML_INDENTATION,
                QCMAT_KEY_SYMTYPE,
                QCMAT_KVAL_SKEW);
        break;
    case QNONSYMMAT:
        fprintf(fp_mat,
                "%s  %s: %s\n",
                QCMATRIX_YAML_INDENTATION,
                QCMAT_KEY_SYMTYPE,
                QCMAT_KVAL_GENERAL);
        break;
    case QSYMMAT:
        fprintf(fp_mat,
                "%s  %s: %s\n",
                QCMATRIX_YAML_INDENTATION,
                QCMAT_KEY_SYMTYPE,
                QCMAT_KVAL_SYMMETRIC);
        break;
    default:
        printf("QcMatWrite>> symmetry type %d\n", A->sym_type);
        QErrorExit(FILE_AND_LINE, "invalid symmetry type");
    }
    fprintf(fp_mat,
            "%s  %s: %"QINT_FMT"\n",
            QCMATRIX_YAML_INDENTATION,
            QCMAT_KEY_NUM_BLOCKS,
            A->num_blocks);
    fprintf(fp_mat,
            "%s  %s:\n",
            QCMATRIX_YAML_INDENTATION,
            QCMAT_KEY_BLOCKS);
    for (irow=0; irow<A->num_blocks; irow++) {
        for (icol=0; icol<A->num_blocks; icol++) {
            if (A->assembled[irow][icol]==QTRUE) {
                fprintf(fp_mat,
                        "%s  %s- %s: [%"QINT_FMT", %"QINT_FMT"]\n",
                        QCMATRIX_YAML_INDENTATION,
                        QCMATRIX_YAML_INDENTATION,
                        QCMAT_KEY_IDX_BLOCK,
                        irow,
                        icol);
                err_code = CmplxMatWrite(&A->blocks[irow][icol],
                                         fp_mat,
                                         view_option);
                QErrorCheckCode(err_code, FILE_AND_LINE, "calling CmplxMatWrite()");
            }
        }
    }
    return QSUCCESS;
}
