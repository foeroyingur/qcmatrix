/*
   QcMatrix: an abstract matrix library
   Copyright 2012-2015 Bin Gao

   QcMatrix is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   QcMatrix is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with QcMatrix. If not, see <http://www.gnu.org/licenses/>.

   This file implements the functions of Fortran 90 APIs.

   2014-03-11, Bin Gao:
   * generated by tools/qcmatrix_fortran.py
*/

#include "qcmatrix.h"
/* uses CMake generated header file with auto-detected mangling */
#include "f90_mangling.h"

/* To be consistent with QcMat type defined in f90_api_f.F90, the struct QcMat_ptr
   only has one member as the QcMat pointer, so that type(QcMat) can be converted
   to struct QcMat_ptr directly, inspired by the PETSc library */
typedef struct {
    QcMat *f90_mat;
} QcMat_ptr;

QVoid f90_api_QcMatCreate(QcMat_ptr *A, QErrorCode *ierr)
{
    A->f90_mat = (QcMat *)malloc(sizeof(QcMat));
    if (A->f90_mat==NULL) {
        printf("f90_api_QcMatCreate>> failed to allocate memory for A->f90_mat\n");
#if defined(QCMATRIX_AUTO_ERROR_EXIT)
        exit(QFAILURE);
#else
        *ierr = QFAILURE;
#endif
    }
    else {
        *ierr = QcMatCreate(A->f90_mat);
    }
}

QVoid f90_api_QcMatDestroy(QcMat_ptr *A, QErrorCode *ierr)
{
    *ierr = QcMatDestroy(A->f90_mat);
    free(A->f90_mat);
    A->f90_mat = NULL;
}

QVoid f90_api_QcMatBlockCreate(QcMat_ptr *A, const QInt *num_blocks, QErrorCode *ierr)
{
    *ierr = QcMatBlockCreate(A->f90_mat, *num_blocks);
}

QVoid f90_api_QcMatSetSymType(QcMat_ptr *A, const QInt *sym_type, QErrorCode *ierr)
{
    /* defined symmetry types, to be consistent with include/api/qcmatrix_f_mat_symmetry.h90 */
    QcSymType def_sym_type[3]={QANTISYMMAT,QNONSYMMAT,QSYMMAT};
    *ierr = QcMatSetSymType(A->f90_mat, def_sym_type[*sym_type+1]);
}

QVoid f90_api_QcMatSetDataType(QcMat_ptr *A,
                               const QInt *num_block_idx,
                               const QInt *idx_block_row,
                               const QInt *idx_block_col,
                               const QInt *data_type,
                               QErrorCode *ierr)
{
    /* defined data types, to be consistent with include/api/qcmatrix_f_mat_data.h90 */
    QcDataType def_data_type[4]={QIMAGMAT,QCMPLXMAT,QREALMAT,QNULLMAT};
    QcDataType *c_data_type;
    QInt iblk;
    c_data_type = (QcDataType *)malloc((*num_block_idx)*sizeof(QcDataType));
    if (c_data_type==NULL) {
        printf("f90_api_QcMatSetDataType>> failed to allocate memory for c_data_type\n");
#if defined(QCMATRIX_AUTO_ERROR_EXIT)
        exit(QFAILURE);
#else
        *ierr = QFAILURE;
#endif
    }
    else {
        for (iblk=0; iblk<(*num_block_idx); iblk++) {
            c_data_type[iblk] = def_data_type[data_type[iblk]+1];
        }
        *ierr = QcMatSetDataType(A->f90_mat,
                                 *num_block_idx,
                                 idx_block_row,
                                 idx_block_col,
                                 c_data_type);
        free(c_data_type);
        c_data_type = NULL;
    }
}

QVoid f90_api_QcMatSetDimMat(QcMat_ptr *A,
                             const QInt *num_row,
                             const QInt *num_col,
                             QErrorCode *ierr)
{
    *ierr = QcMatSetDimMat(A->f90_mat, *num_row, *num_col);
}

#if defined(QCMATRIX_STORAGE_MODE)
/* FIXME: converts QInt to QcStorageMode */
QVoid f90_api_QcMatSetStorageMode(QcMat_ptr *A,
                                  const QcStorageMode *storage_mode,
                                  QErrorCode *ierr)
{
    *ierr = QcMatSetStorageMode(A->f90_mat, *storage_mode);
}
#endif

QVoid f90_api_QcMatAssemble(QcMat_ptr *A, QErrorCode *ierr)
{
    *ierr = QcMatAssemble(A->f90_mat);
}

QVoid f90_api_QcMatGetNumBlocks(QcMat_ptr *A, QInt *num_blocks, QErrorCode *ierr)
{
    *ierr = QcMatGetNumBlocks(A->f90_mat, num_blocks);
}

QVoid f90_api_QcMatGetSymType(QcMat_ptr *A, QInt *sym_type, QErrorCode *ierr)
{
    /* converts symmetry types, to be consistent with
       include/api/qcmatrix_f_mat_symmetry.h90 and include/types/mat_symmetry.h */
    QInt def_sym_type[3]={-1,0,1};
    QcSymType c_sym_type;
    *ierr = QcMatGetSymType(A->f90_mat, &c_sym_type);
    *sym_type = def_sym_type[c_sym_type+1];
}

QVoid f90_api_QcMatGetDataType(QcMat_ptr *A,
                               const QInt *num_block_idx,
                               const QInt *idx_block_row,
                               const QInt *idx_block_col,
                               QInt *data_type,
                               QErrorCode *ierr)
{
    /* defined data types, to be consistent with
       include/api/qcmatrix_f_mat_data.h90 and include/types/mat_data.h */
    QInt def_data_type[4]={-1,0,1,2};
    QcDataType *c_data_type;
    QInt iblk;
    c_data_type = (QcDataType *)malloc((*num_block_idx)*sizeof(QcDataType));
    if (c_data_type==NULL) {
        printf("f90_api_QcMatGetDataType>> failed to allocate memory for c_data_type\n");
#if defined(QCMATRIX_AUTO_ERROR_EXIT)
        exit(QFAILURE);
#else
        *ierr = QFAILURE;
#endif
    }
    else {
        *ierr = QcMatGetDataType(A->f90_mat,
                                 *num_block_idx,
                                 idx_block_row,
                                 idx_block_col,
                                 c_data_type);
        for (iblk=0; iblk<(*num_block_idx); iblk++) {
            data_type[iblk] = def_data_type[c_data_type[iblk]+1];
        }
        free(c_data_type);
        c_data_type = NULL;
    }
}

QVoid f90_api_QcMatGetDimMat(QcMat_ptr *A,
                             QInt *num_row,
                             QInt *num_col,
                             QErrorCode *ierr)
{
    *ierr = QcMatGetDimMat(A->f90_mat, num_row, num_col);
}

#if defined(QCMATRIX_STORAGE_MODE)
/* FIXME: converts QInt to QcStorageMode */
QVoid f90_api_QcMatGetStorageMode(QcMat_ptr *A,
                                  QcStorageMode *storage_mode,
                                  QErrorCode *ierr)
{
    *ierr = QcMatGetStorageMode(A->f90_mat, storage_mode);
}
#endif

QVoid f90_api_QcMatIsAssembled(QcMat_ptr *A, QInt *assembled, QErrorCode *ierr)
{
    /* Boolean type, to be consistent with
       include/api/qcmatrix_f_boolean.h90 and include/types/qcmatrix_basic_types.h */
    QInt f_Boolean[2]={0,1};
    QBool c_assembled;
    *ierr = QcMatIsAssembled(A->f90_mat, &c_assembled);
    *assembled = f_Boolean[c_assembled];
}

QVoid f90_api_QcMatSetRealValues(QcMat_ptr *A,
                                 const QInt *idx_block_row,
                                 const QInt *idx_block_col,
                                 const QInt *idx_first_row,
                                 const QInt *num_row_set,
                                 const QInt *idx_first_col,
                                 const QInt *num_col_set,
                                 const QReal *values,
                                 QErrorCode *ierr)
{
    *ierr = QcMatSetValues(A->f90_mat,
                           *idx_block_row,
                           *idx_block_col,
                           *idx_first_row,
                           *num_row_set,
                           *idx_first_col,
                           *num_col_set,
                           values,
                           NULL);
}

QVoid f90_api_QcMatSetImagValues(QcMat_ptr *A,
                                 const QInt *idx_block_row,
                                 const QInt *idx_block_col,
                                 const QInt *idx_first_row,
                                 const QInt *num_row_set,
                                 const QInt *idx_first_col,
                                 const QInt *num_col_set,
                                 const QReal *values,
                                 QErrorCode *ierr)
{
    *ierr = QcMatSetValues(A->f90_mat,
                           *idx_block_row,
                           *idx_block_col,
                           *idx_first_row,
                           *num_row_set,
                           *idx_first_col,
                           *num_col_set,
                           NULL,
                           values);
}

QVoid f90_api_QcMatAddRealValues(QcMat_ptr *A,
                                 const QInt *idx_block_row,
                                 const QInt *idx_block_col,
                                 const QInt *idx_first_row,
                                 const QInt *num_row_add,
                                 const QInt *idx_first_col,
                                 const QInt *num_col_add,
                                 const QReal *values,
                                 QErrorCode *ierr)
{
    *ierr = QcMatAddValues(A->f90_mat,
                           *idx_block_row,
                           *idx_block_col,
                           *idx_first_row,
                           *num_row_add,
                           *idx_first_col,
                           *num_col_add,
                           values,
                           NULL);
}

QVoid f90_api_QcMatAddImagValues(QcMat_ptr *A,
                                 const QInt *idx_block_row,
                                 const QInt *idx_block_col,
                                 const QInt *idx_first_row,
                                 const QInt *num_row_add,
                                 const QInt *idx_first_col,
                                 const QInt *num_col_add,
                                 const QReal *values,
                                 QErrorCode *ierr)
{
    *ierr = QcMatAddValues(A->f90_mat,
                           *idx_block_row,
                           *idx_block_col,
                           *idx_first_row,
                           *num_row_add,
                           *idx_first_col,
                           *num_col_add,
                           NULL,
                           values);
}

QVoid f90_api_QcMatGetRealValues(QcMat_ptr *A,
                                 const QInt *idx_block_row,
                                 const QInt *idx_block_col,
                                 const QInt *idx_first_row,
                                 const QInt *num_row_get,
                                 const QInt *idx_first_col,
                                 const QInt *num_col_get,
                                 QReal *values,
                                 QErrorCode *ierr)
{
    *ierr = QcMatGetValues(A->f90_mat,
                           *idx_block_row,
                           *idx_block_col,
                           *idx_first_row,
                           *num_row_get,
                           *idx_first_col,
                           *num_col_get,
                           values,
                           NULL);
}

QVoid f90_api_QcMatGetImagValues(QcMat_ptr *A,
                                 const QInt *idx_block_row,
                                 const QInt *idx_block_col,
                                 const QInt *idx_first_row,
                                 const QInt *num_row_get,
                                 const QInt *idx_first_col,
                                 const QInt *num_col_get,
                                 QReal *values,
                                 QErrorCode *ierr)
{
    *ierr = QcMatGetValues(A->f90_mat,
                           *idx_block_row,
                           *idx_block_col,
                           *idx_first_row,
                           *num_row_get,
                           *idx_first_col,
                           *num_col_get,
                           NULL,
                           values);
}

QVoid f90_api_QcMatDuplicate(QcMat_ptr *A,
                             const QInt *duplicate_option,
                             QcMat_ptr *B,
                             QErrorCode *ierr)
{
    /* defined duplicate option, to be consistent with
       include/api/qcmatrix_f_mat_duplicate.h90 and include/types/mat_duplicate.h */
    QcDuplicateOption def_dup_option[2]={COPY_PATTERN_ONLY,COPY_PATTERN_AND_VALUE};
    *ierr = QcMatDuplicate(A->f90_mat, def_dup_option[*duplicate_option], B->f90_mat);
}

QVoid f90_api_QcMatZeroEntries(QcMat_ptr *A, QErrorCode *ierr)
{
    *ierr = QcMatZeroEntries(A->f90_mat);
}

QVoid f90_api_QcMatGetTrace(QcMat_ptr *A,
                            const QInt *num_diag_blocks,
                            QReal *trace,
                            QErrorCode *ierr)
{
    *ierr = QcMatGetTrace(A->f90_mat, *num_diag_blocks, trace);
}

QVoid f90_api_QcMatGetMatProdTrace(QcMat_ptr *A,
                                   QcMat_ptr *B,
                                   const QInt *op_B,
                                   const QInt *num_diag_blocks,
                                   QReal *trace,
                                   QErrorCode *ierr)
{
    /* defined operations on the matrix, to be consistent with
       include/api/qcmatrix_f_mat_operations.h90 and include/types/mat_operations.h */
    QcMatOperation def_mat_op[4]={MAT_NO_OPERATION,
                                  MAT_TRANSPOSE,
                                  MAT_HERM_TRANSPOSE,
                                  MAT_COMPLEX_CONJUGATE};
    *ierr = QcMatGetMatProdTrace(A->f90_mat,
                                 B->f90_mat,
                                 def_mat_op[*op_B],
                                 *num_diag_blocks,
                                 trace);
}

#if defined(QCMATRIX_ENABLE_VIEW)
QVoid f90_api_QcMatWrite(QcMat_ptr *A,
                         const QChar *mat_label,
                         const QInt *view_option,
                         QErrorCode *ierr)
{
    /* defined view option, to be consistend with
       include/api/qcmatrix_f_mat_view.h90 and include/types/mat_view.h */
    QcViewOption def_view_option[2]={BINARY_VIEW,ASCII_VIEW};
    *ierr = QcMatWrite(A->f90_mat, mat_label, def_view_option[*view_option]);
}

QVoid f90_api_QcMatRead(QcMat_ptr *A,
                        const QChar *mat_label,
                        const QInt *view_option,
                        QErrorCode *ierr)
{
    /* defined view option, to be consistend with
       include/api/qcmatrix_f_mat_view.h90 and include/types/mat_view.h */
    QcViewOption def_view_option[2]={BINARY_VIEW,ASCII_VIEW};
    *ierr = QcMatRead(A->f90_mat, mat_label, def_view_option[*view_option]);
}
#endif

QVoid f90_api_QcMatScale(const QReal *scal_number, QcMat_ptr *A, QErrorCode *ierr)
{
    *ierr = QcMatScale(scal_number, A->f90_mat);
}

QVoid f90_api_QcMatAXPY(const QReal *multiplier,
                        QcMat_ptr *X,
                        QcMat_ptr *Y,
                        QErrorCode *ierr)
{
    *ierr = QcMatAXPY(multiplier, X->f90_mat, Y->f90_mat);
}

QVoid f90_api_QcMatTranspose(const QInt *op_A,
                             QcMat_ptr *A,
                             QcMat_ptr *B,
                             QErrorCode *ierr)
{
    /* defined operations on the matrix, to be consistent with
       include/api/qcmatrix_f_mat_operations.h90 and include/types/mat_operations.h */
    QcMatOperation def_mat_op[4]={MAT_NO_OPERATION,
                                  MAT_TRANSPOSE,
                                  MAT_HERM_TRANSPOSE,
                                  MAT_COMPLEX_CONJUGATE};
    *ierr = QcMatTranspose(def_mat_op[*op_A], A->f90_mat, B->f90_mat);
}

QVoid f90_api_QcMatGEMM(const QInt *op_A,
                        const QInt *op_B,
                        const QReal *alpha,
                        QcMat_ptr *A,
                        QcMat_ptr *B,
                        const QReal *beta,
                        QcMat_ptr *C,
                        QErrorCode *ierr)
{
    /* defined operations on the matrix, to be consistent with
       include/api/qcmatrix_f_mat_operations.h90 and include/types/mat_operations.h */
    QcMatOperation def_mat_op[4]={MAT_NO_OPERATION,
                                  MAT_TRANSPOSE,
                                  MAT_HERM_TRANSPOSE,
                                  MAT_COMPLEX_CONJUGATE};
    *ierr = QcMatGEMM(def_mat_op[*op_A],
                      def_mat_op[*op_B],
                      alpha,
                      A->f90_mat,
                      B->f90_mat,
                      beta,
                      C->f90_mat);
}

#if defined(ADAPTER_F90_LANG)
#if defined(ADAPTER_BLOCK_CMPLX)
QVoid f90_api_QcMatSetAdapterMat(QcMat_ptr *A, QInt *iA, QErrorCode *ierr)
{
    *ierr = AdapterMatSetExternalMat(A->f90_mat, iA);
}

QVoid f90_api_QcMatGetAdapterMat(QcMat_ptr *A, QInt *iA, QErrorCode *ierr)
{
    *ierr = AdapterMatGetExternalMat(A->f90_mat, iA);
}
#elif defined(ADAPTER_BLOCK_REAL)
QVoid f90_api_QcMatSetAdapterMat(QcMat_ptr *A,
                                 const QInt *data_type,
                                 QInt *iA,
                                 QErrorCode *ierr)
{
    RealMat *A_adapter;
    /* defined data types, to be consistent with include/api/qcmatrix_f_mat_data.h90 */
    QcDataType def_data_type[4]={QIMAGMAT,QCMPLXMAT,QREALMAT,QNULLMAT};
    *ierr = QcMatSetAdapterMat(A->f90_mat, def_data_type[*data_type+1], &A_adapter);
    if (*ierr==QSUCCESS) {
        *ierr = AdapterMatSetExternalMat(A_adapter, iA);
    }
    A_adapter = NULL;
}

QVoid f90_api_QcMatGetAdapterMat(QcMat_ptr *A,
                                 const QInt *data_type,
                                 QInt *iA,
                                 QErrorCode *ierr)
{
    RealMat *A_adapter;
    /* defined data types, to be consistent with include/api/qcmatrix_f_mat_data.h90 */
    QcDataType def_data_type[4]={QIMAGMAT,QCMPLXMAT,QREALMAT,QNULLMAT};
    *ierr = QcMatGetAdapterMat(A->f90_mat, def_data_type[*data_type+1], &A_adapter);
    if (*ierr==QSUCCESS) {
        *ierr = AdapterMatGetExternalMat(A_adapter, iA);
    }
    A_adapter = NULL;
}
#elif defined(ADAPTER_CMPLX_MAT)
QVoid f90_api_QcMatSetAdapterMat(QcMat_ptr *A,
                                 const QInt *idx_block_row,
                                 const QInt *idx_block_col,
                                 QInt *iA,
                                 QErrorCode *ierr)
{
    CmplxMat *A_adapter;
    *ierr = QcMatSetAdapterMat(A->f90_mat, *idx_block_row, *idx_block_col, &A_adapter);
    if (*ierr==QSUCCESS) {
        *ierr = AdapterMatSetExternalMat(A_adapter, iA);
    }
    A_adapter = NULL;
}

QVoid f90_api_QcMatGetAdapterMat(QcMat_ptr *A,
                                 const QInt *idx_block_row,
                                 const QInt *idx_block_col,
                                 QInt *iA,
                                 QErrorCode *ierr)
{
    CmplxMat *A_adapter;
    *ierr = QcMatGetAdapterMat(A->f90_mat, *idx_block_row, *idx_block_col, &A_adapter);
    if (*ierr==QSUCCESS) {
        *ierr = AdapterMatGetExternalMat(A_adapter, iA);
    }
    A_adapter = NULL;
}
#elif defined(ADAPTER_REAL_MAT)
QVoid f90_api_QcMatSetAdapterMat(QcMat_ptr *A,
                                 const QInt *idx_block_row,
                                 const QInt *idx_block_col,
                                 const QInt *data_type,
                                 QInt *iA,
                                 QErrorCode *ierr)
{
    RealMat *A_adapter;
    /* defined data types, to be consistent with include/api/qcmatrix_f_mat_data.h90 */
    QcDataType def_data_type[4]={QIMAGMAT,QCMPLXMAT,QREALMAT,QNULLMAT};
    *ierr = QcMatSetAdapterMat(A->f90_mat,
                               *idx_block_row,
                               *idx_block_col,
                               def_data_type[*data_type+1],
                               &A_adapter);
    if (*ierr==QSUCCESS) {
        *ierr = AdapterMatSetExternalMat(A_adapter, iA);
    }
    A_adapter = NULL;
}

QVoid f90_api_QcMatGetAdapterMat(QcMat_ptr *A,
                                 const QInt *idx_block_row,
                                 const QInt *idx_block_col,
                                 const QInt *data_type,
                                 QInt *iA,
                                 QErrorCode *ierr)
{
    RealMat *A_adapter;
    /* defined data types, to be consistent with include/api/qcmatrix_f_mat_data.h90 */
    QcDataType def_data_type[4]={QIMAGMAT,QCMPLXMAT,QREALMAT,QNULLMAT};
    *ierr = QcMatGetAdapterMat(A->f90_mat,
                               *idx_block_row,
                               *idx_block_col,
                               def_data_type[*data_type+1],
                               &A_adapter);
    if (*ierr==QSUCCESS) {
        *ierr = AdapterMatGetExternalMat(A_adapter, iA);
    }
    A_adapter = NULL;
}
#endif
#endif

QVoid f90_api_QcMatMatCommutator(QcMat_ptr *A, QcMat_ptr *B, QcMat_ptr *C, QErrorCode *ierr)
{
    *ierr = QcMatMatCommutator(A->f90_mat, B->f90_mat, C->f90_mat);
}

QVoid f90_api_QcMatMatSCommutator(QcMat_ptr *A,
                                  QcMat_ptr *B,
                                  QcMat_ptr *S,
                                  QcMat_ptr *C,
                                  QErrorCode *ierr)
{
    *ierr = QcMatMatSCommutator(A->f90_mat, B->f90_mat, S->f90_mat, C->f90_mat);
}

QVoid f90_api_QcMatMatHermCommutator(QcMat_ptr *A,
                                     QcMat_ptr *B,
                                     QcMat_ptr *C,
                                     QErrorCode *ierr)
{
    *ierr = QcMatMatHermCommutator(A->f90_mat, B->f90_mat, C->f90_mat);
}

QVoid f90_api_QcMatMatSHermCommutator(QcMat_ptr *A,
                                      QcMat_ptr *B,
                                      QcMat_ptr *S,
                                      QcMat_ptr *C,
                                      QErrorCode *ierr)
{
    *ierr = QcMatMatSHermCommutator(A->f90_mat, B->f90_mat, S->f90_mat, C->f90_mat);
}

QVoid f90_api_QcMatSetRandMat(QcMat_ptr *A,
                              const QInt *sym_type,
                              const QInt *data_type,
                              const QInt *num_blocks,
                              const QInt *num_row,
                              const QInt *num_col,
                              QErrorCode *ierr)
{
    /* defined symmetry types, to be consistent with include/api/qcmatrix_f_mat_symmetry.h90 */
    QcSymType def_sym_type[3]={QANTISYMMAT,QNONSYMMAT,QSYMMAT};
    /* defined data types, to be consistent with include/api/qcmatrix_f_mat_data.h90 */
    QcDataType def_data_type[4]={QIMAGMAT,QCMPLXMAT,QREALMAT,QNULLMAT};
    *ierr = QcMatSetRandMat(A->f90_mat,
                            def_sym_type[*sym_type+1],
                            def_data_type[*data_type+1],
                            *num_blocks,
                            *num_row,
                            *num_col);
}

QVoid f90_api_QcMatIsEqual(QcMat_ptr *A,
                           QcMat_ptr *B,
                           const QInt *cf_values,
                           QInt *is_equal,
                           QErrorCode *ierr)
{
    /* Boolean type, to be consistent with
       include/api/qcmatrix_f_boolean.h90 and include/types/qcmatrix_basic_types.h */
    QBool c_Boolean[2]={QFALSE,QTRUE};
    QInt f_Boolean[2]={0,1};
    QBool c_is_equal;
    *ierr = QcMatIsEqual(A->f90_mat, B->f90_mat, c_Boolean[*cf_values], &c_is_equal);
    *is_equal = f_Boolean[c_is_equal];
}

QVoid f90_api_QcMatCfArray(QcMat_ptr *A,
                           const QInt *row_major,
                           const QInt *size_values,
                           const QReal *values_real,
                           const QReal *values_imag,
                           QInt *is_equal,
                           QErrorCode *ierr)
{
    /* Boolean type, to be consistent with
       include/api/qcmatrix_f_boolean.h90 and include/types/qcmatrix_basic_types.h */
    QBool c_Boolean[2]={QFALSE,QTRUE};
    QInt f_Boolean[2]={0,1};
    QBool c_is_equal;
    *ierr = QcMatCfArray(A->f90_mat,
                         c_Boolean[*row_major],
                         *size_values,
                         values_real,
                         values_imag,
                         &c_is_equal);
    *is_equal = f_Boolean[c_is_equal];
}

QVoid f90_api_QcMatGetAllValues(QcMat_ptr *A,
                                const QInt *row_major,
                                const QInt *size_values,
                                QReal *values_real,
                                QReal *values_imag,
                                QErrorCode *ierr)
{
    /* Boolean type, to be consistent with
       include/api/qcmatrix_f_boolean.h90 and include/types/qcmatrix_basic_types.h */
    QBool c_Boolean[2]={QFALSE,QTRUE};
    *ierr = QcMatGetAllValues(A->f90_mat,
                              c_Boolean[*row_major],
                              *size_values,
                              values_real,
                              values_imag);
}
