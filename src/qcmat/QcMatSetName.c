/* QcMatrix: an abstract matrix library
   Copyright 2012-2015 Bin Gao

   QcMatrix is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   QcMatrix is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with QcMatrix. If not, see <http://www.gnu.org/licenses/>.

   This file implements the function QcMatSetName().

   2016-10-31, Bin Gao:
   * first version
*/

#include "qcmatrix.h"

/*@% \brief sets the name of a matrix
     \author Bin Gao
     \date 2016-10-31
     \param[QcMat:struct]{in} A the matrix, should be at least assembled by
         QcMatAssemble()
     \param[char:char]{in} mat_name name of the matrix, should be unique
     \return[QErrorCode:int] error information
*/
QErrorCode QcMatSetName(QcMat *A, const char *mat_name)
{
    QInt num_blocks;           /* number of blocks of the matrix */
    QInt num_digits;           /* number of digits of the number of blocks */
    QSizeT len_name;           /* length of the matrix name */
    QSizeT len_tag_delimiter;  /* length of tag of the delimiter */
    QSizeT len_tag_mat_block;  /* length of tag of blocks */
    QSizeT len_block_name;     /* length of the block name */
    QSizeT len_indices;        /* length of the label of the row and column indices */
    char *block_name;          /* name of each block as name+TAG_MAT_BLOCK %
                                  +TAG_DELIMITER+"row_index"+TAG_DELIMITER+"colum_index" */
    QSizeT len_row_label;      /* length of the label of the rows */
    char *row_label;           /* label of rows as "row_index" */
    QInt irow, icol;
    QErrorCode err_code;
    /* checks the number of blocks */
    if (A->num_blocks<1) {
        printf("QcMatSetName>> number of blocks %"QINT_FMT"\n", A->num_blocks);
        QErrorExit(FILE_AND_LINE, "invalid number of blocks");
    }
    if (A->name!=NULL) {
        free(A->name);
    }
    len_name = strlen(mat_name);
    A->name = (char *)malloc(len_name+1);
    if (A->name==NULL) {
        printf("QcMatSetName>> given matrix name (%"QINT_FMT"): %s\n",
               (QInt)len_name,
               mat_name);
        QErrorExit(FILE_AND_LINE, "failed to allocate memory for matrix name");
    }
    strcpy(A->name, mat_name);
    /* gets the number of digits of the number of blocks */
    num_blocks = A->num_blocks;
    num_digits = 0;
    while (num_blocks!=0)
    {
        num_blocks /= 10;
        num_digits++;
    }
    /* length of the tag of the delimiter, without the zero-terminator */
    len_tag_delimiter = strlen(TAG_DELIMITER);
    /* length of the tag of blocks, without the zero-terminator */
    len_tag_mat_block = strlen(TAG_MAT_BLOCK);
    /* length of the label for the blocks, +1 for the zero-terminator */
    len_indices = 2*(num_digits+len_tag_delimiter)+1;
    len_block_name = len_name+len_tag_mat_block+len_indices;
    block_name = (char *)malloc(len_block_name);
    if (block_name==NULL) {
        printf("QcMatSetName>> length of label for the blocks %"QINT_FMT"\n",
               (QInt)len_block_name);
        QErrorExit(FILE_AND_LINE, "failed to allocate memory for block name");
    }
    /* prepares the label for the blocks of the matrix as
       name+TAG_MAT_BLOCK+TAG_DELIMITER */
    memcpy(block_name, mat_name, len_name);
    memcpy(block_name+len_name, TAG_MAT_BLOCK, len_tag_mat_block);
    len_block_name = len_name+len_tag_mat_block;
    memcpy(block_name+len_block_name, TAG_DELIMITER, len_tag_delimiter);
    len_block_name += len_tag_delimiter;
    /* allocates memory for the label of the rows, +1 for the zero-terminator */
    row_label = (char *)malloc(num_digits+len_tag_delimiter+1);
    if (row_label==NULL) {
        printf("QcMatSetName>> number of blocks %"QINT_FMT" (%"QINT_FMT")\n",
               A->num_blocks,
               num_digits);
        QErrorExit(FILE_AND_LINE, "failed to allocate memory for row label");
    }
    /* sets name of each block */
    for (irow=0; irow<A->num_blocks; irow++) {
        /* generates the label of this row "row_index" */
        snprintf(row_label, num_digits+1, "%"QINT_FMT"", irow);
        len_row_label = strlen(row_label);
        memcpy(row_label+len_row_label, TAG_DELIMITER, len_tag_delimiter);
        len_row_label += len_tag_delimiter;
        /* label for the blocks becomes
           name+TAG_MAT_BLOCK+TAG_DELIMITER+"row_index"+TAG_DELIMITER */
        memcpy(block_name+len_block_name, row_label, len_row_label);
        len_block_name += len_row_label;
        /* left length for the label of the columns */
        len_name = len_indices-len_row_label;
        for (icol=0; icol<A->num_blocks; icol++) {
            /* appends the label of this column */
            snprintf(&block_name[len_block_name],
                     len_name,
                     "%"QINT_FMT"",
                     icol);
            err_code = CmplxMatSetName(&A->blocks[irow][icol], block_name);
            QErrorCheckCode(err_code, FILE_AND_LINE, "calling CmplxMatSetName()");
        }
        /* changes the length of the label of blocks */
        len_block_name -= len_row_label;
    }
    /* cleans */
    free(row_label);
    row_label = NULL;
    free(block_name);
    block_name = NULL;
    return QSUCCESS;
}
