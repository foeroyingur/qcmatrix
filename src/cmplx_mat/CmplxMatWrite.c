/* QcMatrix: an abstract matrix library
   Copyright 2012-2015 Bin Gao

   QcMatrix is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   QcMatrix is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with QcMatrix. If not, see <http://www.gnu.org/licenses/>.

   This file implements the function CmplxMatWrite().

   2012-04-04, Bin Gao:
   * first version
*/

/* we will implement functions of square block complex matrix if external
   library has implemented real square block matrix */
#if defined(ADAPTER_BLOCK_REAL)
#include "qcmatrix.h"
#define CmplxMatWrite QcMatWrite
#else
#include "impls/cmplx_mat.h"
#endif

/* some basic algebraic functions */
#include "utilities/qcmatrix_algebra.h"

/*% \brief writes a matrix to file
    \author Bin Gao
    \date 2012-04-04
    \param[CmplxMat:struct]{in} A the matrix, should be at least assembled
        by CmplxMatAssemble()
    \param[QcViewOption:int]{in} view_option option of writing, see file
        include/types/mat_view.h
    \return[QErrorCode:int] error information
*/
QErrorCode CmplxMatWrite(CmplxMat *A,
                         FILE *fp_mat,
                         const QcViewOption view_option)
{
    QErrorCode err_code;
#if defined(QCMATRIX_ENABLE_HDF5)
    /* variables for HDF5 library */
    hid_t file_id;        /* identifier of the QCMATRIX_FILE */
    hid_t dataspace_id ;  /* identifier of the data space */
    hid_t dataset_id;     /* identifier of the dataset */
    /*herr_t err_hdf5;*/      /* error code for the HDF5 */
#endif
#if defined(QCMATRIX_ENABLE_MXML)
    /* variables for Mini-XML library */
#endif
    switch (view_option) {
    case BINARY_VIEW:
#if defined(QCMATRIX_ENABLE_HDF5)
        /* opens the QCMATRIX_FILE */
        if (access(QCMATRIX_FILE, F_OK)==0) {
            file_id = H5Fopen(QCMATRIX_FILE, H5F_ACC_RDWR, H5P_DEFAULT);
        }
        /* creates the QCMATRIX_FILE */
        else {
            file_id = H5Fcreate(QCMATRIX_FILE, H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT);
        }
        /* creates the data space for the dataset */
        dataspace_id = H5Screate(H5S_SCALAR);
        /* the matrix has been written before, so we open the dataset A->name in group "/" */
        if (H5Lexists(file_id, A->name, H5P_DEFAULT)!=QFALSE) {
            dataset_id = H5Dopen(file_id, A->name, H5P_DEFAULT);
        }
        /* the matrix is written for the first time, we create a dataset A->name in group "/" */
        else {
            dataset_id = H5Dcreate(file_id,
                                   A->name,
                                   H5T_NATIVE_INT,
                                   dataspace_id,
                                   H5P_DEFAULT,
                                   H5P_DEFAULT,
                                   H5P_DEFAULT);
        }
        /* writes the data type of the complex matrix */
        H5Dwrite(dataset_id,
                 H5T_NATIVE_INT,
                 H5S_ALL,
                 H5S_ALL,
                 H5P_DEFAULT,
                 &A->data_type);
        /* closes the data space, dataset and the QCMATRIX_FILE */
        H5Sclose(dataspace_id);
        H5Dclose(dataset_id);
        H5Fclose(file_id);
#else
        fwrite(&A->data_type, sizeof(A->data_type), 1, fp_mat);
#endif
        break;
    case ASCII_VIEW:
#if defined(QCMATRIX_ENABLE_MXML)
#else
        //fprintf(fp_mat, "%d\n", A->data_type);
#endif
        break;
    default:
        printf("CmplxMatWrite>> view option: %d\n", view_option);
        QErrorExit(FILE_AND_LINE, "invalid view option");
    }
    switch (A->data_type) {
    case QREALMAT:
        fprintf(fp_mat,
                "%s  %s  %s:\n",
                QCMATRIX_YAML_INDENTATION,
                QCMATRIX_YAML_INDENTATION,
                CMPLXMAT_KEY_REAL);
        err_code = RealMatWrite(&A->cmplx_mat[A->real_part],
                                fp_mat,
                                view_option);
        QErrorCheckCode(err_code, FILE_AND_LINE, "calling RealMatWrite(R)");
        break;
    case QIMAGMAT:
        fprintf(fp_mat,
                "%s  %s  %s:\n",
                QCMATRIX_YAML_INDENTATION,
                QCMATRIX_YAML_INDENTATION,
                CMPLXMAT_KEY_IMAG);
        err_code = RealMatWrite(&A->cmplx_mat[A->imag_part],
                                fp_mat,
                                view_option);
        QErrorCheckCode(err_code, FILE_AND_LINE, "calling RealMatWrite(I)");
        break;
    case QCMPLXMAT:
        fprintf(fp_mat,
                "%s  %s  %s:\n",
                QCMATRIX_YAML_INDENTATION,
                QCMATRIX_YAML_INDENTATION,
                CMPLXMAT_KEY_REAL);
        err_code = RealMatWrite(&A->cmplx_mat[A->real_part],
                                fp_mat,
                                view_option);
        QErrorCheckCode(err_code, FILE_AND_LINE, "calling RealMatWrite(R)");
        fprintf(fp_mat,
                "%s  %s  %s:\n",
                QCMATRIX_YAML_INDENTATION,
                QCMATRIX_YAML_INDENTATION,
                CMPLXMAT_KEY_IMAG);
        err_code = RealMatWrite(&A->cmplx_mat[A->imag_part],
                                fp_mat,
                                view_option);
        QErrorCheckCode(err_code, FILE_AND_LINE, "calling RealMatWrite(I)");
        break;
    default:
        printf("CmplxMatWrite>> data type of matrix A: %d\n", A->data_type);
        QErrorExit(FILE_AND_LINE, "invalid data type");
    }
    return QSUCCESS;
}
