
/* CmplxMatrix: an abstract matrix library
   Copyright 2012-2015 Bin Gao

   CmplxMatrix is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   CmplxMatrix is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with CmplxMatrix. If not, see <http://www.gnu.org/licenses/>.

   This file implements the function CmplxMatSetName().

   2016-10-31, Bin Gao:
   * first version
*/

/* we will implement functions of square block complex matrix if external
   library has implemented real square block matrix */
#if defined(ADAPTER_BLOCK_REAL)
#include "qcmatrix.h"
#define CmplxMatSetName QcMatSetName
#else
#include "impls/cmplx_mat.h"
#endif

/* some basic algebraic functions */
#include "utilities/qcmatrix_algebra.h"

/*@% \brief sets the name of a matrix
     \author Bin Gao
     \date 2016-10-31
     \param[CmplxMat:struct]{in} A the matrix, should be at least assembled by
         CmplxMatAssemble()
     \param[char:char]{in} name mat_name of the matrix, should be unique
     \return[QErrorCode:int] error information
*/
QErrorCode CmplxMatSetName(CmplxMat *A, const char *mat_name)
{
    char *name_real_mat;      /* name of the real or imaginary parts */
    QSizeT len_name;          /* length of the matrix name */
    QSizeT len_tag_real_mat;  /* length of the tag of the real part */
    QSizeT len_tag_imag_mat;  /* length of the tag of the imaginary part */
    QErrorCode err_code;
    if (A->name!=NULL) {
        free(A->name);
    }
    len_name = strlen(mat_name);
    A->name = (char *)malloc(len_name+1);
    if (A->name==NULL) {
        printf("CmplxMatSetName>> given matrix name (%"QINT_FMT"): %s\n",
               (QInt)len_name,
               mat_name);
        QErrorExit(FILE_AND_LINE, "failed to allocate memory for matrix name");
    }
    strcpy(A->name, mat_name);
    /* get the names for the real and imaginary parts of the matrix */
    len_tag_real_mat = strlen(TAG_REAL_MAT)+1;  /* +1 for the zero-terminator */
    len_tag_imag_mat = strlen(TAG_IMAG_MAT)+1;
    name_real_mat = (char *)malloc(len_name+QMax(len_tag_real_mat,len_tag_imag_mat));
    if (name_real_mat==NULL) {
        printf("CmplxMatSetName>> given matrix name (%"QINT_FMT"): %s\n",
               (QInt)len_name,
               mat_name);
        QErrorExit(FILE_AND_LINE, "failed to allocate memory for name_real_mat");
    }
    memcpy(name_real_mat, A->name, len_name);
    memcpy(name_real_mat+len_name, TAG_REAL_MAT, len_tag_real_mat);
    err_code = RealMatSetName(&A->cmplx_mat[A->real_part], name_real_mat);
    QErrorCheckCode(err_code, FILE_AND_LINE, "calling RealMatSetName(Real)");
    memcpy(name_real_mat+len_name, TAG_IMAG_MAT, len_tag_imag_mat);
    err_code = RealMatSetName(&A->cmplx_mat[A->imag_part], name_real_mat);
    QErrorCheckCode(err_code, FILE_AND_LINE, "calling RealMatSetName(Imag)");
    free(name_real_mat);
    name_real_mat = NULL;
    return QSUCCESS;
}
