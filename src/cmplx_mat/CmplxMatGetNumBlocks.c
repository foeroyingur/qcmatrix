/* QcMatrix: an abstract matrix library
   Copyright 2012-2015 Bin Gao

   QcMatrix is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   QcMatrix is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public License
   along with QcMatrix. If not, see <http://www.gnu.org/licenses/>.

   This file implements the function QcMatGetNumBlocks().

   2012-04-04, Bin Gao:
   * first version
*/

#include "qcmatrix.h"

/*% \brief gets the number of blocks
    \author Bin Gao
    \date 2012-04-04
    \param[QcMat:struct]{in} A the matrix, should be at least created by
        QcMatCreate() and QcMatBlockCreate()
    \param[QInt:int]{out} num_blocks the number of blocks
    \return[QErrorCode:int] error information
*/
QErrorCode QcMatGetNumBlocks(QcMat *A, QInt *num_blocks)
{
    QInt imag_num_blocks;
    QErrorCode err_code;
    /* the numbers of blocks of the real and imaginary parts should be the same */
    err_code = RealMatGetNumBlocks(&A->cmplx_mat[A->real_part], num_blocks);
    QErrorCheckCode(err_code, FILE_AND_LINE, "calling RealMatGetNumBlocks");
    err_code = RealMatGetNumBlocks(&A->cmplx_mat[A->imag_part], &imag_num_blocks);
    QErrorCheckCode(err_code, FILE_AND_LINE, "calling RealMatGetNumBlocks");
    if (*num_blocks==0) {
        *num_blocks = imag_num_blocks;
    }
    else if ((*num_blocks!=imag_num_blocks) && (imag_num_blocks!=0)) {
        printf("number of blocks of the real part %"QINT_FMT"\n", *num_blocks);
        printf("number of blocks of the imaginary part %"QINT_FMT"\n", imag_num_blocks);
        QErrorExit(FILE_AND_LINE, "invalid number of blocks");
    }
    return QSUCCESS;
}
